# Цифровая платформа i4.0

> Тестовое задание для одной секретной компании

Дан макет в виде JPG, реализовано в виде SPA, на frontend-фреймворке Vue.js/Nuxtjs, Element UI

[Просмотреть ДЕМО](https://webstu.gitlab.io/test/#/)
